package com.api.rest.Controller;

import com.api.rest.Entity.Publicacion;
import com.api.rest.Repository.publicacionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/publicacion")
public class PublicacionController {

    @Autowired
    private publicacionRepository publicacionRepository;

    @RequestMapping(
            value = "/all",
            method = RequestMethod.GET,
            produces = "application/json")
    public ResponseEntity<List<Publicacion>> getPublicaciones(){
        List<Publicacion> publicacions = publicacionRepository.findAll();
        return ResponseEntity.ok(publicacions);
    }

    @RequestMapping(value = "{publicacionId}",
            method = RequestMethod.GET,
            produces = "application/json")
    public ResponseEntity<Publicacion> getPublicacionById(@PathVariable Integer publicacionId){
        Optional<Publicacion> publicacionOptional = publicacionRepository.findById(publicacionId);
        return publicacionOptional.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.noContent().build());
    }

    @RequestMapping(
            value = "/create",
            method = RequestMethod.POST,
            produces = "application/json")
    public ResponseEntity<Publicacion> createPublicacion(@RequestBody Publicacion publicacion){
        Publicacion publicacion1 = publicacionRepository.save(publicacion);
        return ResponseEntity.ok(publicacion1);
    }

    @RequestMapping(
            value = "/update",
            method = RequestMethod.PUT,
            produces = "application/json")
    public ResponseEntity<Publicacion> updateUser(@RequestBody Publicacion publicacion){
        Optional<Publicacion> publicacionOptional = publicacionRepository.findById(publicacion.getId());
        if(publicacionOptional.isPresent()) {
            Publicacion updatePublicacion = publicacionOptional.get();
            updatePublicacion.setImagen(publicacion.getImagen());
            updatePublicacion.setDescripcion(publicacion.getDescripcion());
            updatePublicacion.setTiempo(publicacion.getTiempo());
            publicacionRepository.save(updatePublicacion);
            return ResponseEntity.ok(updatePublicacion);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(
            value = "{publicacionId}",
            method = RequestMethod.DELETE,
            produces = "application/json")
    public ResponseEntity<Void> deleteMessage(@PathVariable Integer publicacionId){
        publicacionRepository.deleteById(publicacionId);
        return ResponseEntity.ok(null);
    }

}
