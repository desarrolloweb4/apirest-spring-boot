package com.api.rest.Repository;

import com.api.rest.Entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;

public interface messageRepository extends JpaRepository<Message, Integer> {
}
