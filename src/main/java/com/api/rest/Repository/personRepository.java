package com.api.rest.Repository;

import com.api.rest.Entity.User;

import org.springframework.data.jpa.repository.JpaRepository;

public interface personRepository extends JpaRepository<User,Integer> {
}
