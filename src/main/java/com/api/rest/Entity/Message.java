package com.api.rest.Entity;

import org.hibernate.annotations.CreationTimestamp;

import java.sql.Timestamp;

//import lombok.AllArgsConstructor;
//import lombok.Getter;
//import lombok.NoArgsConstructor;
//import lombok.Setter;

import javax.persistence.*;

//@NoArgsConstructor
//@AllArgsConstructor
//@Getter
//@Setter
@Entity
@Table(name = "messages")
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false)
    private String message;
	private String descripcion;
    @CreationTimestamp
    private Timestamp tiempo;
    
    public Message() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Timestamp getTiempo() {
		return tiempo;
	}

	public void setTiempo(Timestamp tiempo) {
		this.tiempo = tiempo;
	}
}
