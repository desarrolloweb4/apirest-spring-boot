package com.api.rest.Controller;

import com.api.rest.Entity.Message;
import com.api.rest.Repository.messageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/message")
public class MessageController {

    @Autowired
    private messageRepository messageRepository;

    @RequestMapping(
            value = "/all",
            method = RequestMethod.GET,
            produces = "application/json")
    public ResponseEntity<List<Message>> getMessage(){
        List<Message> messages = messageRepository.findAll();
        return ResponseEntity.ok(messages);
    }

    @RequestMapping(value = "{messageId}",
            method = RequestMethod.GET,
            produces = "application/json")
    public ResponseEntity<Message> getMessageById(@PathVariable Integer messageId){
        Optional<Message> productOptional = messageRepository.findById(messageId);
        return productOptional.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.noContent().build());
    }

    @RequestMapping(
            value = "/create",
            method = RequestMethod.POST,
            produces = "application/json")
    public ResponseEntity<Message> createMessage(@RequestBody Message message){
        Message mensaje = messageRepository.save(message);
        return ResponseEntity.ok(mensaje);
    }

    @RequestMapping(
            value = "/update",
            method = RequestMethod.PUT,
            produces = "application/json")
    public ResponseEntity<Message> updateMessage(@RequestBody Message message){
        Optional<Message> messageOptional = messageRepository.findById(message.getId());
        if(messageOptional.isPresent()) {
            Message updateMessage = messageOptional.get();
            updateMessage.setMessage(message.getMessage());
            updateMessage.setTiempo(message.getTiempo());
            return ResponseEntity.ok(updateMessage);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(
            value = "{messageId}",
            method = RequestMethod.DELETE,
            produces = "application/json")
    public ResponseEntity<Void> deleteMessage(@PathVariable Integer messageId){
        messageRepository.deleteById(messageId);
        return ResponseEntity.ok(null);
    }

}
