package com.api.rest.Repository;

import com.api.rest.Entity.Publicacion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface publicacionRepository extends JpaRepository<Publicacion, Integer> {
}
