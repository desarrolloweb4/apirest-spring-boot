package com.api.rest.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.api.rest.Entity.User;
import com.api.rest.Repository.personRepository;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
    private personRepository personRepository;

    @RequestMapping(
            value = "/all",
            method = RequestMethod.GET,
            produces = "application/json")
    public ResponseEntity<List<User>> getUsers(){
        List<User> users = personRepository.findAll();
        return ResponseEntity.ok(users);
    }

    @RequestMapping(value = "{userId}",
            method = RequestMethod.GET,
            produces = "application/json")
    public ResponseEntity<User> getUserById(@PathVariable Integer userId){
        Optional<User> userOptional = personRepository.findById(userId);
        return userOptional.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.noContent().build());
    }

    @RequestMapping(
            value = "/create",
            method = RequestMethod.POST,
            produces = "application/json")
    public ResponseEntity<User> createUser(@RequestBody User person){
    	User user = personRepository.save(person);
        return ResponseEntity.ok(user);
    }

    @RequestMapping(
            value = "/update",
            method = RequestMethod.PUT,
            produces = "application/json")
    public ResponseEntity<User> updateUser(@RequestBody User person){
        Optional<User> userOptional = personRepository.findById(person.getId());
        if(userOptional.isPresent()) {
        	User updateUser = userOptional.get();
        	updateUser.setNombre(person.getNombre());
        	updateUser.setApellido(person.getApellido());
        	updateUser.setNickname(person.getNickname());
        	updateUser.setCorreo(person.getCorreo());
        	updateUser.setPassword(person.getPassword());
        	updateUser.setRepeat(person.getRepeat());
        	updateUser.setImagen(person.getImagen());
            return ResponseEntity.ok(updateUser);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(
            value = "{userId}",
            method = RequestMethod.DELETE,
            produces = "application/json")
    public ResponseEntity<Void> deleteMessage(@PathVariable Integer userId){
        personRepository.deleteById(userId);
        return ResponseEntity.ok(null);
    }

}
